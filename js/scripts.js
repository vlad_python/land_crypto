var fsgo = 0,btnIdx = null;

$(document).ready(function () {

    function modalClose(modalId) {
        $('#' + modalId).modal('hide');
    }

    function modalOpen(modalId) {
        $('#' + modalId).modal('show');
    }

    jQuery.validator.addMethod("phoneUS", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            (phone_number.match(/^((\+7|7|8)+([0-9]){10})$/) || phone_number.match(/^(\+380|0)[0-9]{9}$/));

    }, "Пожалуйста укажите телефон в международном формате");

    jQuery.validator.addMethod("validate_email", function (value, element) {
        if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Введите верный Email адрес");


    $('.requestCode').click(function (e) {
        requestSms($('#contactForm').serialize());
    });

    function requestSms(params) {
        $.post('/', params, function (data) {
                if (data.step) {
                    if (isNoSmsAccept()) {
                        successAccept(data);
                    } else {
                        $('#sms_phone').html(data.phone);
                        modalClose('formModal');
                        modalOpen('smsModal');

                        $('.repeat-code').show();
                        $('.repeat-code-btn').hide();

                       // setTimeout(function () {
                           $('#getting-started').countdown(new Date(data.lastAttempt)).on('update.countdown', function (event) {
                               $(this).text(
                                   event.strftime('%M:%S')
                               );
                           }).on('finish.countdown', function (event) {
                               $('.repeat-code').hide();
                               $('.repeat-code-btn').show();
                           });
                       // }, 1500);

                    }
                } else {
                    $('.errors').html('Не верный формат номера').css('color', 'red');
                    console.log(data, 'valid number example 9521234567')
                }
            }, 'json'
        );
    }

    function primary_save() {
        if ($('.form_email').val().length == 0) {
            return;
        }
        $.ajax({
            type: "POST",
            url: '/',
            data: {
                name: $('.form_name').val(),
                email: $('.form_email').val(),
                phone: $('.form_phone').val()
//                hash: $('.response_hash').val()
            }
        });
    }

//    $('#b_sms_change').click(function () {
//
//        modalClose('smsModal');
////        $('#smsModal').modal('hide');
//        $('#sms_error').html('');
//        $('#sms_code').val('');
////        $('#formModal').modal('show');
//        modalOpen('formModal');
//    });

    $('#b_sms_ok').click(function () {
        $('#sms_error').html('');
        var code = $('#sms_code').val();
        $.post('/', {phone_confirm: code}, function (data) {
            if (data.status === 'ok') {
                successAccept(data);
            } else {
                $('.errors').html('Не верный код SMS').css('color', 'red');
            }
        }, 'json');
        //}
    });

    $('#contactForm').submit(function (e) {
        e.preventDefault();
        var form = $(this);

        form.validate({
            rules: {
                email: {
                    validate_email: true
                },
                phone: {
                    phoneUS: true,
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages:{
                name:"Пожалуйста укажите ваше имя"
            }
        });
        if (form.valid()) {
            if (fsgo === 0) {
                var p = $(this).serialize();
                $('#form_error').html('');
                requestSms(p);
                return false;
            }
        }
    });

    function successAccept(data) {
        setTimeout(function () {
            //alert('переход');
          window.location = data.url
        }, 150);
    }

    function isFormAccepted() {
        return $('#formModal').hasClass('accepted')
    }

    function isNoSmsAccept() {
        return $('#formModal').hasClass('no-sms-accept');
    }

    // jQuery(document).ajaxError(function (e, xhr) {
    //     if (xhr && xhr.responseText) {
    //         var errors = JSON.parse(xhr.responseText);
    //         $.each(errors.errors, function (k, v) {
    //             $('.form_' + k).parent().removeClass('has-success').addClass('has-error');
    //         });
    //     }
    // });

    $('#formModal').on('shown.bs.modal', function (e) {
        btnIdx = e.relatedTarget.getAttribute('id');
        $('input.btnIdx').val(btnIdx);
    });

    $('#formModal').on('hidden.bs.modal', function (e) {
        $('input.btnIdx').val(btnIdx);
    });

});